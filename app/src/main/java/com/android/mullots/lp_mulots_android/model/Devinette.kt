package com.android.mullots.lp_mulots_android.model

import android.media.Image


/**
 * @id is use as identifier to identify Devinette instance
 * @title --
 * @img will be drawn if it's set else it's only a colr
 * @color will be drawn if no <code>img</code> is set
 * @words is the list off words that user can tell and w'll be validate
 * @difficulty is the amount of point w'll be add to score in case of success --> also used to identify in level easy/medium/hard
 */
data class Devinette (val id:Int, val title:String, val img:Image?, val color:String, val words:List<String>, val difficulty:Int) {

}